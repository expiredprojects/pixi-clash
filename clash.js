/*global PIXI, requestAnimFrame, document*/

var width = 800;
var height = 600;

var stage = new PIXI.Stage(0x667788);
var renderer = PIXI.autoDetectRenderer(width, height);

var cosmos = PIXI.Texture.fromImage("assets/cosmos.png");
var mine = PIXI.Texture.fromImage("assets/mine_o.png");
var elements = [];

function createElement(texture, x, y) {
	'use strict';
	
	var e = new PIXI.Sprite(texture);
	e.position.x = x;
	e.position.y = y;
	return e;
}

function populateCosmos() {
	'use strict';
	
	var c, i, j, cosmosWidth = cosmos.baseTexture.width, cosmosHeight = cosmos.baseTexture.height;
	
	for (i = 0; i < width; i = i + cosmosWidth) {
		for (j = 0; j < height; j = j + cosmosHeight) {
			stage.addChild(createElement(cosmos, i, j));
		}
	}
}

function addElements(number) {
	'use strict';
	
	var i, x, y;
	
	for (i = 0; i < number; i = i + 1) {
		x = Math.random() * width;
		y = Math.random() * height;
		elements.push(createElement(mine, x, y));
	}
}

function tick() {
	'use strict';
	
    requestAnimFrame(tick);
    renderer.render(stage);
}

function init() {
	'use strict';
	
	populateCosmos();
	
	document.body.appendChild(renderer.view);
	requestAnimFrame(tick);
}

init();